﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeros
{
    public class SpiderMan : Human, IClimb
    {
        public int Speed { get; private set; }
        public SpiderMan(string name, int age, int speed) : base(age)
        {
            this.Name = name;
            this.Speed = speed;
        }
        protected override string Name { get ; set ; }

        public void ActivateSuperPowers()
        {
            Climb();
        }

        public void Climb()
        {
            Console.WriteLine("Hero is climbing");
        }
        public override string ToString()
        {
            return base.ToString() + $"Speed:{this.Speed}";
        }
    }
}
