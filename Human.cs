﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeros
{
    public abstract class Human
    {
        protected abstract string Name { get; set; }
        public int Age { get; private set; }
        public Human(int age)
        {
            //this.Name = name;
            this.Age = age;
        }
        public override string ToString()
        {
            return $"Name: {this.Name} Age: {this.Age} ";
        }
    }
}
