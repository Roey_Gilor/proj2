﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeros
{
    public class Flash : Human, IFlash
    {
        protected override string Name { get; set ; }
        public int Voltage { get; private set; }
        public Flash(string name, int age, int voltage) : base(age)
        {
            this.Name = name;
            this.Voltage = voltage;
        }
        public void ActivateSuperPowers()
        {
            FireLightnings();
        }

        public void FireLightnings()
        {
            Console.WriteLine("Hero is firing");
        }
        public override string ToString()
        {
            return base.ToString() + $"Voltage:{this.Voltage}";
        }
    }
}
