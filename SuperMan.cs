﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeros
{
    public class SuperMan : Human, IFly
    {
        public SuperMan(string name, int age, int webLeft) : base(age)
        {
            this.Name = name;
            this.WebLeft = webLeft;
        }
        public int WebLeft { get; private set; }
        protected override string Name { get; set; }

        public void ActivateSuperPowers()
        {
            Fly();
        }

        public void Fly()
        {
            Console.WriteLine("Hero is flying");
        }
        public override string ToString()
        {
            string st= base.ToString() + $"Web left: {this.WebLeft}";
            return st;
        }
    }
}
