﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeros
{
    
    class Program
    {
        static void ActivateHero(ISuperHero superHero)
        {
            superHero.ActivateSuperPowers();
        }
        static void IdentifyHero(ISuperHero superHero)
        {
            if (superHero is SuperMan)
                Console.WriteLine("Superman detected");
            else if (superHero is SpiderMan)
                Console.WriteLine("Spiderman detected");
            else
                Console.WriteLine("Flash detected");
        }
        static void GetMoreHeroData(ISuperHero superHero)
        {
            object realHero;
            if (superHero is SuperMan)
                realHero = superHero as SuperMan;
            else if (superHero is SpiderMan)
                realHero = superHero as SpiderMan;
            else
                realHero = superHero as Flash;
            Console.WriteLine(realHero.ToString());
        }
        static ISuperHero CreateHero (string hero)
        {
            if (hero == "SuperMan")
            {
                SuperMan superman = new SuperMan("super man", 20, 5);
                return superman;
            }
            if (hero == "SpiderMan")
            {
                SpiderMan spiderMan = new SpiderMan("spider man", 30, 500);
                return spiderMan;
            }
            if (hero == "Flash")
            {
                Flash flash = new Flash("flash", 25, 1000);
                return flash;
            }
            return null;
        }
        static void Main(string[] args)
        {
            SuperMan superman = new SuperMan("super man", 20, 5);
            SpiderMan spiderMan = new SpiderMan("spider man", 30, 500);
            Flash flash = new Flash("flash", 25, 1000);
            ISuperHero[] heroes = new ISuperHero[]
            {
                superman, spiderMan, flash
            };
            foreach (var hero in heroes)
            {
                ActivateHero(hero);
            }
            foreach (var hero in heroes)
            {
                IdentifyHero(hero);
            }
            foreach (var hero in heroes)
            {
                GetMoreHeroData(hero);
            }
            Console.WriteLine(CreateHero("SuperMan").ToString());
            Console.WriteLine(CreateHero("SpiderMan").ToString());
            Console.WriteLine(CreateHero("Flash").ToString());
        }
    }
}
